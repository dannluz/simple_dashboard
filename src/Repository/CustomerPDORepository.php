<?php

namespace Repository;


use App\AbstractPDORepository;

class CustomerPDORepository extends AbstractPDORepository
{
    /**
     * @param array $request
     * @return mixed
     */
    public function getAll(array $request)
    {
        $sql = "SELECT * FROM simple_dashboard.customer";
        $query = $this->db->prepare($sql);

        $query->execute();

        return $query->fetchAll();
    }
}
