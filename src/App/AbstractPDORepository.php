<?php

namespace App;

use PDO;


abstract class AbstractPDORepository implements RepositoryInterface
{
    protected $db = null;

    function __construct()
    {
        try {
            self::openDatabaseConnection();
        } catch (\PDOException $e) {
            exit('Database connection could not be established.');
        }
    }

    public function openDatabaseConnection()
    {
        $options = array(PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ, PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING);

        $this->db = new PDO(DB_TYPE . ':host=' . DB_HOST . ';dbname=' . DB_NAME , DB_USER, DB_PASS, $options);
    }

    abstract public function getAll(array $request);

    public function whereClause(array $request)
    {
        $where = "WHERE true ";
        if (empty($request['from']) && empty($request['to'])) {
            $where .= ' and date(purchase_date) BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE() ';

            return [
                'where' => $where,
                'parameters' => [],
            ];
        }

        $dateFrom = \DateTime::createFromFormat('d-m-Y', $request['from']);
        $dateFrom = $dateFrom->format('Y-m-d');

        $dateTo = \DateTime::createFromFormat('d-m-Y', $request['to']);
        $dateTo = $dateTo->format('Y-m-d');

        $where = "WHERE true ";
        $parameters = [];

        if ($request['from']) {
            $where .= "AND date(purchase_date) >= :dateFrom ";
            $parameters[':dateFrom'] = $dateFrom;
        }

        if ($request['to']) {
            $where .= "AND date(purchase_date) <= :dateTo ";
            $parameters[':dateTo'] = $dateTo;
        }

        return [
            'where' => $where,
            'parameters' => $parameters,
        ];
    }


}