<!-- Fixed navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Sales Dashboard</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <form class="navbar-form navbar-right form-inline" action="index.php" method="get">
                <label class="mr-sm-2" for="from">From</label>
                <input type="text" id="from" name="from" class="form-control" readonly value=""  >
                <label class="mr-sm-2" for="to">to</label>
                <input type="text" id="to" name="to" class="form-control" readonly value="">
                <button type="submit" class="btn btn-default">Go</button>
            </form>
        </div><!--/.nav-collapse -->
    </div>
</nav>

<div class="container">

    <!-- Main component for a primary marketing message or call to action -->
    <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

    <div class="row placeholders">
        <div class="col-xs-6 col-sm-3 placeholder widget">
            <h4><?php echo $data['totalOrders']; ?></h4>
            <span class=""> Orders</span>
        </div>
        <div class="col-xs-6 col-sm-3 placeholder widget">
            <h4>$ <?php echo $data['totalRevenue']; ?></h4>
            <span class="">Revenues</span>
        </div>
        <div class="col-xs-6 col-sm-3 placeholder widget">
            <h4><?php echo $data['totalCustomers']; ?></h4>
            <span class="">Customers</span>
        </div>
    </div>

</div> <!-- /container -->