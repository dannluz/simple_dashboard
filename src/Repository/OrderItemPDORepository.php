<?php

namespace Repository;


use App\AbstractPDORepository;

class OrderItemPDORepository extends AbstractPDORepository
{
    /**
     * @param array $request
     * @return mixed
     */
    public function getAll(array $request)
    {
        $sql = "SELECT * FROM simple_dashboard.order_items";
        $query = $this->db->prepare($sql);

        $parameters = [];

        $query->execute($parameters);

        return $query->fetchAll();
    }

    /**
     * @param array $request
     * @return mixed
     */
    public function getNumberRevenue(array $request)
    {
        $whereClause = $this->whereClause($request);

        $sql = "SELECT SUM(oi.price)  as total_revenue 
                FROM order_items oi
                INNER JOIN simple_dashboard.order o ON o.id = oi.order_id
                {$whereClause['where']};";

        $query = $this->db->prepare($sql);

        $parameters = $whereClause['parameters'];

        $query->execute($parameters);

        return $query->fetch();
    }

}
