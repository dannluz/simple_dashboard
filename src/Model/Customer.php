<?php

namespace Model;

use App\RepositoryInterface;

class Customer
{

    /**
     * @var RepositoryInterface
     */
    protected $repository;

    /**
     * Order constructor.
     * @param $repository
     */
    public function __construct(RepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function getAll(array $request)
    {
        return $this->repository->getAll($request);
    }

}
