<?php

namespace View;

class View
{
    /**
     * @param $template
     * @param $data
     */
    public function render($template, $data)
    {
        require __DIR__ . "/../View/header.php";
        require __DIR__ . "/../View/{$template}";
        require __DIR__ . "/../View/footer.php";
    }
}