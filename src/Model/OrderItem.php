<?php

namespace Model;


use App\RepositoryInterface;

class OrderItem
{

/**
* @var RepositoryInterface
*/
    protected $repository;

    /**
     * OrderItem constructor.
     * @param $repository
     */
    public function __construct(RepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $request
     * @return array
     */
    public function getAll(array $request)
    {
        return $this->repository->getAll($request);
    }

    /**
     * @param array $request
     * @return mixed
     */
    public function getNumberRevenue(array $request)
    {
        return $this->repository->getNumberRevenue($request);

    }
}
