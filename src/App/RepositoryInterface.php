<?php

namespace App;


interface RepositoryInterface
{
    public function openDatabaseConnection();

    public function getAll(array $request);

    public function whereClause(array $request);

}