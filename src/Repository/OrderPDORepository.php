<?php

namespace Repository;


use App\AbstractPDORepository;

class OrderPDORepository extends AbstractPDORepository
{
    /**
     * @param array $request
     * @return mixed
     */
    public function getAll(array $request)
    {
        $whereClause = $this->whereClause($request);

        $sql = "SELECT * FROM simple_dashboard.order {$whereClause['where']}";
        $query = $this->db->prepare($sql);

        $parameters = $whereClause['parameters'];

        $query->execute($parameters);

        return $query->fetchAll();
    }

    /**
     * @param array $request
     * @return mixed
     */
    public function getNumberOfOrders(array $request)
    {
        $whereClause = $this->whereClause($request);

        $sql = "SELECT count(*) /1000 as orders
                        ,DATE_FORMAT(purchase_date, ' %M %d') as date
                FROM simple_dashboard.order
                {$whereClause['where']}
                GROUP BY date
                ORDER BY purchase_date ASC;";
        $query = $this->db->prepare($sql);

        $parameters = $whereClause['parameters'];

        $query->execute($parameters);

        return $query->fetchAll();
    }

    /**
     * @param array $request
     * @return mixed
     */
    public function getNumberOfCustomerPerDay(array $request)
    {
        $whereClause = $this->whereClause($request);

        $sql = " SELECT count( distinct customer_id) / (SELECT count(id) as customers FROM customer) * 100 as customerPercentage
                        , DATE_FORMAT(purchase_date, ' %M %d') as date
                FROM simple_dashboard.order
                {$whereClause['where']}
                GROUP BY  date
                ORDER BY purchase_date ASC;";
        $query = $this->db->prepare($sql);

        $parameters = $whereClause['parameters'];

        $query->execute($parameters);

        return $query->fetchAll();
    }
}
