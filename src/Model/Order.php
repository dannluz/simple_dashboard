<?php

namespace Model;


use App\RepositoryInterface;

class Order
{
    /**
     * @var RepositoryInterface
     */
    protected $repository;

    /**
     * Order constructor.
     * @param $repository
     */
    public function __construct(RepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $request
     * @return array
     */
    public function getAll(array $request)
    {
        return $this->repository->getAll($request);
    }

    /**
     * @param array $request
     * @return array
     */
    public function getNumberOfOrders(array $request)
    {
        return $this->repository->getNumberOfOrders($request);
    }

    /**
     * @param array $request
     * @return array
     */
    public function getNumberOfCustomerPerDay(array $request)
    {
        return $this->repository->getNumberOfCustomerPerDay($request);
    }

}
