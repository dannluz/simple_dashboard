<?php
namespace Controller;

use Model\Customer;
use Model\Order;
use Model\OrderItem;
use Repository\CustomerPDORepository;
use Repository\OrderItemPDORepository;
use Repository\OrderPDORepository;
use View\View;

class DefaultController
{
    /**
     * @param array $request
     */
    public function index(array $request)
    {

        $order = new Order(new OrderPDORepository());
        $view = new View();
        $orders = $order->getAll($request);
        $numberOfOrders = $order->getNumberOfOrders($request);
        $numberOfCustomersPerDay = $order->getNumberOfCustomerPerDay($request);

        $orderItem = new OrderItem(new OrderItemPDORepository());
        $orderItems = $orderItem->getNumberRevenue($request);

        $customer = new Customer(new CustomerPDORepository());
        $customers = $customer->getAll($request);

        $data = [
            'totalOrders' => count($orders),
            'totalRevenue' => $orderItems->total_revenue,
            'totalCustomers' => count($customers),
            'numberOfOrders' => $numberOfOrders,
            'numberOfCustomersPerDay' => $numberOfCustomersPerDay,
        ];

        return $view->render('default/index.php', $data);
    }
}