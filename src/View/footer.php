<div class="footer">
</div>
<script src="./public/assets/js/jquery.min.js"></script>
<script src="./public/assets/js/jquery-ui.min.js"></script>

<!-- define the project's URL (to make AJAX calls possible, even when using this in sub-folders etc) -->
<script>
    var url = "";
</script>

<script src="./public/assets/js/bootstrap.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script>

    var ordersPerDay = <?php echo json_encode($data['numberOfOrders']); ?>;
    var customersPerDay = <?php echo json_encode($data['numberOfCustomersPerDay']); ?>;

    $(function () {
        datePickerInit();
        highchartsInit();
    });

    function datePickerInit() {
        var dateFormat = "dd-mm-yy";

        $( "#from" ).datepicker();
        $( "#from" ).datepicker( "option", "dateFormat", dateFormat );

        $( "#to" ).datepicker();
        $( "#to" ).datepicker( "option", "dateFormat", dateFormat );

    }

    function highchartsInit() {
        var categories = [];
        var orders = [];
        var customers = [];

        for (var p in ordersPerDay) {
            categories.push(ordersPerDay[p].date);
            orders.push(Number(ordersPerDay[p].orders));
            customers.push(Number(customersPerDay[p].customerPercentage));
        }

        Highcharts.chart('container', {
            chart: {
                zoomType: 'xy'
            },
            title: {
                text: 'Sales Timeframe'
            },
            xAxis: [{
                categories: categories,
                labels: {
                    rotation: -50
                } ,
                crosshair: true
            }],
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value}K',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                },
                title: {
                    text: 'Orders',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                }
            }, { // Secondary yAxis
                title: {
                    text: 'Customers',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                labels: {
                    format: '{value} %',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                opposite: true
            }],
            tooltip: {
                shared: true
            },
            legend: {
                layout: 'vertical',
                align: 'left',
                x: 120,
                verticalAlign: 'top',
                y: 100,
                floating: true,
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
            },
            series: [{
                name: 'Customers',
                type: 'spline',
                yAxis: 1,
                data: customers,
                tooltip: {
                    valueSuffix: ' %'
                }

            }, {
                name: 'Orders',
                type: 'spline',
                data: orders,
                tooltip: {
                    valueSuffix: 'K'
                }
            }]
        });
    }

</script>
</body>
</html>