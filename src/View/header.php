<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Sales Dashboard</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- JS -->
        <!-- please note: The JavaScript files are loaded in the footer to speed up page construction -->

        <!-- CSS -->
        <link href="./public/assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="./public/assets/css/style.css" rel="stylesheet">
        <link href="./public/assets/css/jquery-ui.min.css" rel="stylesheet">

    </head>
    <body>
