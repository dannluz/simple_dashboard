<?php

define('DB_TYPE', 'mysql');
define('DB_HOST', '<your host here>');
define('DB_NAME', 'simple_dashboard');
define('DB_USER', '<your user here>');
define('DB_PASS', '<your pass here>');
define('DB_CHARSET', 'utf8');
