<?php
require 'vendor/autoload.php';
require 'config/config.php';

use Controller\DefaultController;

$controller = new DefaultController();
$controller->index($_GET);
